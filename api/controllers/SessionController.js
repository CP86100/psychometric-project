/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 var bcrypt = require('bcrypt');

module.exports = {
	'new': function(req, res){

		res.locals.flash = _.clone(req.session.flash);
		res.view('session/new');
		req.session.flash = {};
	},

	create: function(req, res, next){
		if(!req.param('username') || !req.param('password')){
			var usernamePasswordRequiredError = [{name: 'usernamePasswordRequiredError', message: 'You must enter both a username and password'}];
			req.session.flash = {
				err: usernamePasswordRequiredError
			}

			res.redirect('/session/new');
			return;
		}

		User.findOne({username: req.param('username')}).exec(function(err, user){
			if(err) return next(err);

			if(!user){
				var noAccountError = [{name: 'noAccountError', message: 'The username '+req.param('username')+' does not exist'}];
				req.session.flash = {
					err: noAccountError
				}
				res.redirect('/session/new');
				return;
			}

			bcrypt.compare(req.param('password'), user.password, function(err, valid){
				if(err) return next(err);

				if(!valid){
					var usernamePasswordMismatchError = [{name: 'usernamePasswordMismatchError', message: 'Invalid username or password'}];
					req.session.flash = {
						err: usernamePasswordMismatchError
					}
					res.redirect('/session/new');
					return;
				}

				req.session.authenticated = true;
				req.session.User = user;

        user.online = true;
        user.save(function(err, user){
          if(err) return next(err);

          if(req.session.User.admin){
            res.redirect('/user');
            return;
          }
          res.redirect('user/show/'+ req.session.User.id)
        });
			});
		});
	},

	destroy: function(req, res, next) {

    User.findOne(req.session.User.id, function foundUser(err, user){
      var admin = false;
      if(req.session.User.admin) admin = true;

      User.update(req.session.User.id,
        { online: false, admin: admin },
        function(err){
          if(err) return next(err);

          req.session.destroy();

          res.redirect('/');
      });
    });
	}
};
