/**
 * TestController
 *
 * @description :: Server-side logic for managing tests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	'new': function(req, res, next){
		res.view();
	},

	create: function(req, res, next){

		Test.create(req.params.all(), function(err, test){
			if(err){
				next(err);
				return res.redirect('/test/new');
			}

			req.session.Test = test;

			res.redirect('/test/show/'+ test.id);
		});
	},

	show: function(req, res, next) {
		Test.findOne(req.param('id'), function(err, test){
			if(err) return next(err);

			res.view({test: test});
		});
	},

	index: function(req, res, next){
		Test.find(function(err, tests){
			if(err) return next(err);

			res.view({tests: tests});
		});
	}

};
