/**
 * Question.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //String that will contain the actual question unless it is a test based on images.
    text: {
      type: 'string'
    },

    //The correct answer to the question if it applies.
    answer: {
      type: 'string'
    },

    //Trait to which the question belongs, used for pschometric tests.
    trait: {
      type: 'string'
    },

    //String of the image path
    image: {
      type: 'string'
    },

    //Question options, such as a,b,c... or very accurate, accurate ...
    options: {
      type: 'array'
    },

    //All the Tests that use this question e.g. a 25 questions and a 100 questions long tests might use the same question(s).
    tests: {
      collection: 'Test',
      via: 'questions'
    }

  }
};
