/**
 * Result.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //The user that achieved the result.
    user: {
      model: 'User',
      via: 'results'
    },

    //The test to which the result corresponds.
    test: {
      model: 'Test',
      via: 'results'
    },

    //The score
    score: {
      type: 'integer',
      required: true
    },

    //Has the result been chared on Facebook?
    shared: {
      type: 'boolean',
      defaultsTo: false
    }
  }
};
