/**
 * Test.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //The name of the test.
    name: {
      type: 'string',
      required: true
    },

    //The image used on a thumbnail.
    image: {
      type: 'string'
    },

    //The questions.
    questions: {
      collection: 'Question',
      via: 'tests'
    },

    //All the existing results.
    results: {
      collection: 'Result',
      via: 'test'
    },

    //All the users that did the test.
    users: {
      collection: 'User',
      via: 'tests'
    }
  }
};
