/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {

    //The unique username.
    "username": {
      type: 'string',
      unique: true,
      required: true
    },

    //The user's email address.
    email: {
      type: 'string',
      required: true,
      email: true,
      unique: true
    },

    //The user's password, stored once encrypted
    password: {
      type: 'string',
      required: true
    },

    //Is the user an admin?
    admin: {
      type: 'boolean',
      defaultsTo: false
    },

    //Is the user online?
    online: {
      type: 'boolean',
      defaultsTo: false
    },

    //All the results the user created.
    results: {
      collection: 'Result',
      via: 'user'
    },

    //All the tests the user did.
    tests: {
      collection: 'Test',
      via: 'users'
    },

    //Make sure some sensible data is not returned for security reasons.
    toJSON: function(){
      var obj = this.toObject();
      delete obj.password;
      delete obj.confirmation;
      delete obj._csrf;

      return obj;
    }
  },

  //This is to update user's admin variable.
  beforeUpdate: function(values, next){
    console.log(values.admin);
    if(values.admin != true){
      values.admin = false;
    }

    next();
  },

  //This is to check if the passwords match on sign-up and then encrypts the password.
  beforeCreate: function (values, next){
    if(!values.password || values.password != values.confirm){
      return next({err: ["Password does not match"]});
    }

    require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword){
      if(err) return next(err);
      values.password = encryptedPassword;
      //values.online = true;
      next();
    });
  }
};
