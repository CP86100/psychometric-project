

module.exports = function(req, res, ok){

  if(req.session.User && req.session.User.admin){
    return ok();
  }
  else{
    var requireAdminError = [{name: 'requireAdminError', message: 'You do not have admin priviledges.'}];
    req.session.flash = {
      err: requireAdminError
    }
    res.redirect('/session/new');
    return;
  }
}
