module.exports = function(req, res, ok){
  var sessionUserMatchesID = req.session.User.id === req.param('id');
  var isAdmin = req.session.User.admin;

  if(!(sessionUserMatchesID || isAdmin)){
    var noRightsError = [{name: 'noRightsError', message: 'You must be either an admin or the owner of the profile.'}];
    req.session.flash = {
      err: noRightsError
    }
    res.redirect('/session/new');
    return;
  }
  ok();

}
